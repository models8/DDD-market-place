
# PRATIQUES ET TECHNOLOGIES  :
- Event Storming
- **Microservices Ready**
- DDD ( Domain Driven Design )
- Clean Architecture/Hexagonal Architecture
- Event-driven architecture
	- Domain Event
	- Integration Event 
	- RabbitMq Message Broker
	- Outbox Pattern (For Domain Events)
- Event Sourcing 
	- EventStore ( EventStoreDb )
	- Projections/ViewModel ( Mongo ) 
	- Snapshot ( Redis ) 
- Api Gateway 
	- API Composition 
	- API Orchestration
	- SAGAS Choreography

Voir les liens vers les tableaux détaillés : 
- [Event Storming](https://miro.com/app/board/o9J_lm_njr8=/?invite_link_id=9283523679156)
- [Schemas d'architecture](https://miro.com/app/board/uXjVOe2lKCk=/?invite_link_id=195201116966)
# EVENT STORMING :

Le plus grand obstacle au DDD est la tendance des développeurs à se concentrer sur les choses qu'ils comprennent extrêmement bien (les concepts de développement logiciel) plutôt que sur le domaine commercial. Un symptôme de ceci peut être observé lorsque des personnes non techniques, telles que des propriétaires de produits ou des défenseurs des utilisateurs, sortent de réunions avec des développeurs et commencent à décrire le système en termes de programmation plutôt qu'en termes commerciaux. Si les développeurs ne comprennent pas le domaine, ils ne peuvent pas le modéliser correctement.

## EVENT STORMING REFLEXION
![](https://github.com/enzo-cora/market-place-DDD/blob/images/event-storming-overview.jpg?raw=true)

## EVENT STORMING PREMIERES CONCLUSION
Cette image représente la conclusion d'un Event Storming de la phase préparatoire de ce projet. 

De ce fait, concernant cette modélisation du domaine, il restera : 
- des points à éclairsir (pas tous marqués) 
- des points à ajouter .
- et d'autres qui viendrons en cours de réalisation suite à de nouvelles exigences des experts métiers.

![](https://github.com/enzo-cora/market-place-DDD/blob/images/event-storming-aggregates.jpg?raw=true)



# STRUCTURE ET ARCHITECTURE 

## Structure du projet

- :heavy_check_mark: **C'est cette structure en couches qui à été adoptée pour ce projet**
```
<bc 1>
|_ domain
|_ application
|_ presentation
|_ infrastructure
<bc 2>
|_ domain
|_ application
|_ presentation
|_ infrastructure
```
A l'intérieur d'un bounded context : 

![](https://github.com/enzo-cora/market-place-DDD/blob/images/files-structure.jpg?raw=true)
- ARCHITECTURE MONOLITHIC NON ADOPTEE : 

Cette architecture est classique et efficace, mais elle n'as pas été selectionnée pour ce projet.
```
domain
|_ <bc 1>
|_ <bc 2>
application
presentation
infrastructure

```

## ARCHITECTURE MICROSERVICE READY 

 :heavy_check_mark: C'est cett architecture qui à été utilisée dans ce "projet démo". Nous sommes dans un bounded-context d'une **MONOLITH-MICROSERVICES-READY**
 
 [Pour voir la version COMPLETE aller ici](https://miro.com/app/board/uXjVOe2lKCk=/?moveToWidget=3458764515506769715&cot=14)
![](https://github.com/enzo-cora/market-place-DDD/blob/images/zoom-event-sourcing-monolith-microservice-ready.jpg?raw=true)



# API GATEWAY : ORCHESTRATION ET COMPOSITION  :
Cette partie concerne **l'API GATEWAY** qui centralise tous les MICROSERVICES via plusieurs méthodes : 
- Api composition
- Materialised view
- Api Orchestration
- Les Sagas

[Voir tous les schema de "Gateway"](https://miro.com/app/board/uXjVOe2lKCk=/?moveToWidget=3458764515497237155&cot=14)
## API COMPOSITION  :

![](https://github.com/enzo-cora/market-place-DDD/blob/images/api-gateway-api-composition.jpg?raw=true)
![](https://github.com/enzo-cora/market-place-DDD/blob/images/api-gateway-materialized-view.jpg?raw=true)

## API ORCHESTRATION  :
![](https://github.com/enzo-cora/market-place-DDD/blob/images/api-gateway-orchestration.jpg?raw=true)

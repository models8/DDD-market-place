import {SalesDomainEventListeners} from "../../infra/entry-points/domain-events/domain-events-listeners/SalesDomainEventListeners";
import {EventDispatcher} from "../../../../_definitions/EventDispatcher/EventDispatcher";
import {EnumSalesDomainEvents} from "../../domain/domain-events/EnumSalesDomainEvents";


type EnumSalesDomainEvents = keyof typeof EnumSalesDomainEvents

export const salesDomainEventDispatcher = new EventDispatcher<EnumSalesDomainEvents>(
  Object.values(EnumSalesDomainEvents)
)

export const initSalesDomainEventListeners = () =>  {
  const salesDomainEventListeners = new SalesDomainEventListeners(salesDomainEventDispatcher)
  salesDomainEventListeners.listen()
}
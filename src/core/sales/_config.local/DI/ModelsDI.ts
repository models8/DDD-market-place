import {EventStoreSales} from "../data-providers/init-sales-event-store-db";
import {CartModel} from "../../infra/data-providers/CartRepository/model/CartModel";
import {ProductModel} from "../../infra/data-providers/ProductRepository/model/ProductModel";


export class ModelsDI {

  static get CartModelInstance(): CartModel {
    return new CartModel(EventStoreSales)
  }

  static get ProductModelInstance() {
    return ProductModel
  }



}

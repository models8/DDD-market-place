import {OnCartValidated} from "../../infra/entry-points/domain-events/domain-event-handlers/OnCartValidated-Handler/OnCartValidated";
import {SalesBrokerChannel} from "../entry-points/init-sales-rabbitmq";
import {
  OnProductAddedToCart
} from "../../infra/entry-points/domain-events/domain-event-handlers/OnProductAddedToCart-Handler/OnProductAddedToCart";
import {cartRM} from "../../infra/data-providers/ReadModels/CartRM/CartRM";

export class DomainEventHandlersDI {

  static get OnProductAddedToCartInstance(): OnProductAddedToCart {
    return new OnProductAddedToCart(
      SalesBrokerChannel,
      cartRM
    )
  }

  static get OnCartValidatedInstance(): OnCartValidated {
    return new OnCartValidated()
  }


}



export type IAddProductToCartRequestDto = {
  authToken? : string,
  sessionId?: string,
  productID : string,
  qty : number
}
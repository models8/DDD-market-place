import {IRemoveProductFromCart } from "./IRemoveProductFromCart";
import {IRemoveProductFromCartResponseDto } from "./IRemoveProductFromCartResponseDto";
import {IRemoveProductFromCartRequestDto } from "./IRemoveProductFromCartRequestDto";
import {Result, success} from "either-result";

export class RemoveProductFromCart implements IRemoveProductFromCart {

  constructor() {
  }

  async execute(requestDTO?: IRemoveProductFromCartRequestDto) : Promise<Result<string, IRemoveProductFromCartResponseDto>>  {
    return success(true)
  }

}
import {ICreateProduct } from "./ICreateProduct";
import {ICreateProductResponseDto } from "./ICreateProductResponseDto";
import {ICreateProductRequestDto } from "./ICreateProductRequestDto";
import {Result, success} from "either-result";

export class CreateProduct implements ICreateProduct {

  constructor() {
  }

  async execute(requestDTO?: ICreateProductRequestDto) : Promise<Result<string, ICreateProductResponseDto>>  {
    return success(true)
  }

}
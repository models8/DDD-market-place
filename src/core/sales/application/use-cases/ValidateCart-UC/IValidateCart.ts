import {IValidateCartResponseDto } from "./IValidateCartResponseDto";
import {IValidateCartRequestDto } from "./IValidateCartRequestDto";
import {IUseCase} from "../../../../../_definitions/IUseCase";

export type IValidateCart = IUseCase<IValidateCartResponseDto,IValidateCartRequestDto>
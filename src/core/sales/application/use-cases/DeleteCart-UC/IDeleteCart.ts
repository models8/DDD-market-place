import {IDeleteCartResponseDto } from "./IDeleteCartResponseDto";
import {IDeleteCartRequestDto } from "./IDeleteCartRequestDto";
import {IUseCase} from "../../../../../_definitions/IUseCase";

export type IDeleteCart = IUseCase<IDeleteCartResponseDto,IDeleteCartRequestDto>
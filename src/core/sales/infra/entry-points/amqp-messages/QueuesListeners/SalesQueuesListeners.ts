import {QueueNames} from "../../../../../../infrastructure/entry-points/RabbitmqBroker/IntegrationEventExchange";
import {
  MessageQueueSubcriber
} from "../../../../../../_definitions/MessageQueueSubscriber/MessageQueueSubcriber";
import {Channel} from "amqplib";
import {IEventHanler} from "../../../../../../_definitions/IEventHanler";

export class SalesQueuesListeners extends MessageQueueSubcriber{

  constructor(
    salesChannel : Channel,
    private onCommandCancelled : IEventHanler,
    private onProductDeleted : IEventHanler,
  ) {
    super(salesChannel)
  }

  async listen(): Promise<void> {
    await Promise.all([
      this.addSubscription(QueueNames.orderCancelled,this.onCommandCancelled),
      this.addSubscription(QueueNames.product_deleted,this.onProductDeleted)
    ])
  }


}

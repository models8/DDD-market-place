import {IHttpResponseContract} from "../../../../../../../../_definitions/ControllerWeb/IHttpResponseContract";

export type IResponseContract = Omit<IHttpResponseContract, "data"|"title">

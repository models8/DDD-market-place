import {IHttpRequestContract} from "../../../../../../../../_definitions/ControllerWeb/IHttpRequestContract";

export type IRequestContract = IHttpRequestContract<
  {
  productId : string
  qty : number
  cartId : string
  },
  undefined,
  undefined,
  {
  authToken? : string
  sessionId?: string
  }>
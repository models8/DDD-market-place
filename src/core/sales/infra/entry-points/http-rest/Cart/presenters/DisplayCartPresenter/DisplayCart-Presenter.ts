import {IDisplayCartResponseDto} from "../../../../../../application/use-cases/DisplayCart-UC/IDisplayCartResponseDto";
import {IDisplayCartPresentationContract} from "./IDisplayCartPresentationContract";
import {IDisplayCartPresenter} from "./IDisplayCart-Presenter";


export class DisplayCartPresenter implements IDisplayCartPresenter{


  async present(responseDto: IDisplayCartResponseDto): Promise<IDisplayCartPresentationContract> {

    const productList : IDisplayCartPresentationContract["productList"] = responseDto.productList.map((prod)=>({
      ...prod,
      priceTTC : prod.priceTTC.toString() + "€/TTC",
      priceHT : prod.priceHT.toString() + "€/HT",
      available : prod.available ? "Available" : "Available soon"
    }))

    return {
      ...responseDto,
      productList,
      totalTTC: responseDto.totalTTC.toString() + "€/TTC",
    }

  }




}
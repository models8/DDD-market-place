import {Document, Model, Schema, Types} from "mongoose";
import {MongoSalesConnection} from "../../../../_config.local/data-providers/init-sales-mongodb";
import {ICartRMProps} from "./ICartRMProps";


type ICartRM =  Model<ICartRMProps&Document>
//-------------Mongodb pair schema ----------------

const schema = new Schema({
  productList :[{
    productId :  {type :Types.ObjectId, required:'Vous devez entrer la description'},
    quantity : {type : Number, required:'Vous devez entrer la quantitée'},
  }],
  cartOwner : {type :Types.ObjectId, required:"Vous devez entrer l'id du propriétaire"},
  category : {type : String, required:'Vous devez entrer la categorie'},
  createdAt : {type : Date, required:'Vous devez entrer la date de creation'},
})

//-------------Create pairModel ----------------

const cartRM : ICartRM = MongoSalesConnection.model('CartsRM', schema)

export {ICartRM,cartRM}
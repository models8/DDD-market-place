import {RepositoryMongo} from "../../../../../../_definitions/Repository/RepositoryMongo";
import {IProductRepository} from "./IProductRepository";
import {IProductDocumentProps} from "../model/ProductModel";
import {IProductEntity} from "../../../../domain/entities/Product-Aggregate/IProductEntity";

export class ProductRepository extends RepositoryMongo<IProductEntity,IProductDocumentProps> implements IProductRepository{

  productExist(productId: string): Promise<boolean> {
    return Promise.resolve(false);
  }

  deleteById(productId: string): Promise<void> {
    return Promise.resolve(undefined);
  }

}
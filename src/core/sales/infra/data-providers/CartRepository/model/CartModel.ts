import ObjectID from "bson-objectid";
import {StreamModel, StreamSchema} from "../../../../../../infrastructure/data-providers/EventStoreORM";
import {Client} from "@eventstore/db-client/dist/Client";


interface ICartDocumentProps {
  _id : string
  productList : Array<[string, number]>
  cartOwner : string
  createdAt : Date
}

class CartModel extends StreamModel<ICartDocumentProps>{

  private static schema : StreamSchema<ICartDocumentProps> = new StreamSchema({
    _id : new ObjectID().toHexString(),
    productList : [],
    cartOwner : new ObjectID().toHexString(),
    createdAt : new Date(),
  })

  constructor(
    private SalesEventStoreCLient : Client,
    eventNameList : string[] = []
  ) {
    super(
      CartModel.schema.properties,
      SalesEventStoreCLient,
      "cart-sales",
      eventNameList
    )
  }

}

export {CartModel,ICartDocumentProps}

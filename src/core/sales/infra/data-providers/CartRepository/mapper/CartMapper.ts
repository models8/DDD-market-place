import {ICartEntity, ICartBuildProps} from "../../../../domain/entities/Cart-Aggregate/ICartEntity";
import {IEntityBuilder} from "../../../../../../_definitions/EntityBuilder/IEntityBuilder";
import {ICartMapper} from "./ICartMapper";
import {ICartDocumentProps} from "../model/CartModel";
import ObjectID from "bson-objectid";
import {Quantity} from "../../../../domain/entities/ValueObjects/Quantity";

export class CartMapper implements ICartMapper {

  constructor(
    private cartbuilder : IEntityBuilder<ICartEntity, ICartBuildProps>
  ) {}

  entityToDocumentProps(entity: ICartEntity) : ICartDocumentProps {
    return entity.lean() as ICartDocumentProps
  }

  documentPropsToEntity(cartDocumentProps: ICartDocumentProps): ICartEntity {
    let entitiyProps = {
      ...cartDocumentProps
    } as unknown as ICartBuildProps

    entitiyProps._id = new ObjectID(cartDocumentProps._id)
    entitiyProps.cartOwner = new ObjectID(cartDocumentProps.cartOwner)
    entitiyProps.productList = new Map()

    cartDocumentProps.productList.map(prodInfo => {
      const id =  new ObjectID(prodInfo[0])
      const qtyResult = Quantity.create(prodInfo[1])
      if(qtyResult.isFailure())
        throw "Database format compatibily problem : DocumentProps does't feet with EntityProps"
      entitiyProps.productList.set(id,qtyResult.value)
    })

    const entity = this.cartbuilder.build(entitiyProps)
    if(entity.isFailure())
      throw "Database format compatibily problem : DocumentProps does't feet with EntityProps"
    return entity.value
  }

}
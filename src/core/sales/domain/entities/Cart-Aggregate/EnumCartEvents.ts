export enum EnumCartEvents {
  cartCreated = "cartCreated",
  productAddedToCart = "productAddedToCart",
  productRemovedFromCart = "productRemovedFromCart",
  productQtyChanged = "productQtyChanged",
  cartValidated = "cartValidated",
  cartDeleted  = "cartDeleted",
}
import {IAggregateRoot} from "../../../../../_definitions/AggregateRoot/IAggregateRoot";
import ObjectID from "bson-objectid";


export interface IProductEntityDto {
  _id : string
  priceTTC : number
  priceHT : number
  category : string
  subCategory : string
  title : string
  description : string
  available : boolean
}

export interface IProductBuildProps {
  _id? : ObjectID
  priceTTC : number
  priceHT : number
  category : string
  subCategory : string
  title : string
  description : string
  available : boolean
}


export interface IProductEntity extends IAggregateRoot{
  lean() : Required<IProductEntityDto>
}


import {ExpressApp} from "../../infrastructure/entry-points/ExpressApp/ExpressApp";
import express, {Router} from "express";
import {IExpressApp} from "../../infrastructure/entry-points/ExpressApp/IExpressApp";


export interface IConfigExpress {
  hostname : string
  port : number
  apiName : string
}

const expressConfig : IConfigExpress = {
  port : process.env.API_PORT ? +process.env.API_PORT : 3001,
  apiName : process.env.API_NAME  || 'api1',
  hostname : process.env.HOSTNAME || "localhost"
}

export const expressAppInit  = () : IExpressApp => new ExpressApp(expressConfig,express(),Router())
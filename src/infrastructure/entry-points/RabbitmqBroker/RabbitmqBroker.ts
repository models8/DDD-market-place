import {Channel, Connection} from "amqplib"
import {IRabbitmqBroker} from "./IRabbitmqBroker";
import {QueueNames, integrationEventExchange, QueuesPatterns} from "./IntegrationEventExchange";


/*--------------------------------- RABBITMQ -----------------------------------*/

export class RabbitmqBroker implements IRabbitmqBroker{

  constructor(
    private connection : Connection
  ) {}

  public async initExchange () : Promise<void> {
    const channel = await this.connection.createChannel()
    await channel.assertExchange(integrationEventExchange, 'topic', {durable: true})

    for (const queue of Object.values(QueueNames)) {
      await channel.assertQueue(queue, {durable: true})
      await channel.bindQueue(queue,integrationEventExchange,QueuesPatterns[queue])
    }
    await channel.close()
  }


  public async createChannel () : Promise<Channel> {
    const channel =   await this.connection.createChannel()
    await channel.prefetch(1)
    return channel
  }

}
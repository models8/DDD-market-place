export interface IStreamSnapshot<data extends Object> {
  streamId : string
  data : data
  revision : bigint
}
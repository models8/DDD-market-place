import {Result} from "either-result";

export interface Receiver<contract,request> {
  checkContractValidity(body: unknown) : Result<string, contract>
  contractToRequestDto(contract: contract) : request
}
import ObjectID from "bson-objectid";
import {EnumIntegrationEvent} from "../../infrastructure/entry-points/RabbitmqBroker/EnumIntegrationEvent";
import {IIntegrationEvent} from "./IIntegrationEvent";

export class IntegrationEvent<Type extends EnumIntegrationEvent, Data extends Object> implements IIntegrationEvent<Type,Data> {

  _id = new ObjectID().toHexString()
  public metadata : Record<string, any>&{dateTimeOccurred: Date}

  constructor(
    public type : Type,
    public data : Data,
    metadata = {}
  ) {
    this.metadata = {
      ...metadata,
      dateTimeOccurred : new Date()
    }
  }
}
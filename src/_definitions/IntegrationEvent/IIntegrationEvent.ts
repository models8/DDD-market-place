import {EnumIntegrationEvent} from "../../infrastructure/entry-points/RabbitmqBroker/EnumIntegrationEvent";

export interface IIntegrationEvent<Type extends EnumIntegrationEvent = EnumIntegrationEvent,  Data extends Record<string, any> = Object> {
  _id : string
  type : Type,
  data : Data,
  metadata : {
    dateTimeOccurred: Date;
    [keyl:string] : any
  }
}

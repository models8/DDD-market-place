import {Result} from "either-result";

interface IEntityBuilder<Entity,EntityProps> {
  instanceOf : any
  build(entitiesDto : EntityProps[]): Result<string[], Entity[]>
  build(entityDto : EntityProps) : Result<string[], Entity>
  build(dto : EntityProps | EntityProps[]) : Result<string[], Entity> | Result<string[], Entity[]>
}

export {IEntityBuilder}
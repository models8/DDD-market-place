import {IEventHanler} from "../IEventHanler";

export interface IMessageQueueSubscriber {
  addSubscription(queue:string, eventHandler: IEventHanler) : Promise<void>
}

export interface IPresenter<ResponseDto,ContractDto> {
  present(responseDto : ResponseDto) : Promise<ContractDto>
}
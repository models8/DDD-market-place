import {IRepository} from "./IRepository";
import {Document, Model} from "mongoose";
import {IEntity} from "../Entity/IEntity";
import {IMapper} from "../IMapper";
import ObjectID from "bson-objectid";
import {Result} from "either-result";

export abstract class RepositoryMongo<Entity extends IEntity, DocumentProps> implements IRepository<Entity> {

  constructor(
    private model: Model<Document & DocumentProps>,
    private mapper: IMapper<Entity, DocumentProps>
  ) {
  }

  async create(newItem: Entity) : Promise<void>  {
    const documentDto :DocumentProps = this.mapper.entityToDocumentProps(newItem)
    await new this.model(documentDto).save()
  }

  async delete(entity: Entity): Promise<void> {
    return "" as any
  }

  async getById(id: ObjectID): Promise<Result<null, Entity>> {
    return "" as any
  }

  async  update(id: string, updatedItem: Entity): Promise<Result<string, true>> {
    return "" as any
  }


}

import {Router} from "express";

export interface IRouterHub {
  router: Router
}

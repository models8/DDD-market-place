import ObjectID from "bson-objectid";
import {IDomainEvent} from "./IDomainEvent";

export class DomainEvent<Type extends string, Data extends Object> implements IDomainEvent<Type,Data> {

  _id = new ObjectID().toHexString()
  public metadata : Record<string, any>&{dateTimeOccurred: Date}

  constructor(
    public type : Type,
    public data : Data,
    metadata = {}
  ) {
    this.metadata = {
      ...metadata,
      dateTimeOccurred : new Date()
    }
  }
}

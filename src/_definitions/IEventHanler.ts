import {IDomainEvent} from "./DomainEvent/IDomainEvent";
import {IIntegrationEvent} from "./IntegrationEvent/IIntegrationEvent";

export interface IEventHanler {
  handle(event : IDomainEvent|IIntegrationEvent) : Promise<void|boolean>
}
import {EventDispatcher} from "./EventDispatcher";
import {IDomainEvent} from "../DomainEvent/IDomainEvent";

enum EnumTestDomainEvents {
  testLaunched = "testLaunched"
}

describe("EventDispatcher Infrastructure",()=> {


  describe("Test 'addListenerMethod()' method", ()=>{

    it('should return success (true) ', () => {
      const eventDispatcher = new EventDispatcher(Object.values(EnumTestDomainEvents))
      const fakeEventListener = {async handle(){}}
      const returnValue = eventDispatcher.addListener(EnumTestDomainEvents.testLaunched, fakeEventListener )
      expect(returnValue).toBe(true)
    })

  })

  describe("Test 'dispatch()' method", ()=>{
    it("should have call the eventListener",()=>  {
      const eventDispatcher = new EventDispatcher(Object.values(EnumTestDomainEvents))
      const mockEventListener = jest.fn(async ()=> {})
      const mockEvent :IDomainEvent = {
        type : EnumTestDomainEvents.testLaunched,
        data : {
          name: "jean",
          firstname : "paul"
        },
        metadata : {
          dateTimeOccurred : new Date()
        }
      }
      eventDispatcher.addListener(EnumTestDomainEvents.testLaunched, {handle : mockEventListener} )
      eventDispatcher.dispatch([mockEvent])

      expect(mockEventListener).toHaveBeenCalled()
    })

  })

})
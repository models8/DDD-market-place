import {IEventDispatcher} from "./IEventDispatcher";
import {IDomainEvent} from "../DomainEvent/IDomainEvent";
import {IEventHanler} from "../IEventHanler";



export class EventDispatcher<DomainEventName extends string> implements IEventDispatcher {


  private eventListeners : Record<DomainEventName, IEventHanler[]>

  constructor(
    domainEventList : DomainEventName[]
  ) {

    const objEventArray = domainEventList.map(eventName  => ({[eventName] : []}))
    this.eventListeners = objEventArray.reduce((prev,curr)=> ({...prev,...curr})) as any
  }


  addListener(eventName: DomainEventName , eventListener : IEventHanler) : boolean {
    const eventlisteners = this.eventListeners[eventName]
    if(eventlisteners){
      eventlisteners.push(eventListener)
      return true
    }
    else
      return false

  }

  dispatch(events: IDomainEvent[]) {
    events.forEach((event) => {
      const eventListeners : IEventHanler[] = this.eventListeners[event.type]
        for (const eventListener of eventListeners) {
          eventListener.handle(event).then()
        }
    })
  }
}
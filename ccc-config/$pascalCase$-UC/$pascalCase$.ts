import {I$pascalCase$ } from "./I$pascalCase$";
import {I$pascalCase$ResponseDto } from "./I$pascalCase$ResponseDto";
import {I$pascalCase$RequestDto } from "./I$pascalCase$RequestDto";
import {Result, success} from "either-result";

export class $pascalCase$ implements I$pascalCase$ {

  constructor() {
  }

  async execute(requestDTO: I$pascalCase$RequestDto) : Promise<Result<string, I$pascalCase$ResponseDto>>  {
    return success(true)
  }

}
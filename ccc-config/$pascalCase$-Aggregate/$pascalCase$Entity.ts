import {I$pascalCase$Entity, I$pascalCase$BuildProps, I$pascalCase$EntityDto} from "./I$pascalCase$Entity";
import {AggregateRoot} from "../../src/_definitions/AggregateRoot/AggregateRoot";
import {EntityBuilder} from "../../src/_definitions/EntityBuilder/EntityBuilder";

class $pascalCase$Builder extends EntityBuilder<I$pascalCase$Entity, I$pascalCase$BuildProps> {

  get instanceOf() : typeof $pascalCase$Entity{
    return $pascalCase$Entity
  }

  protected methodFactory(entity: I$pascalCase$BuildProps): I$pascalCase$Entity {
    return new $pascalCase$Entity(entity)
  }

}

class $pascalCase$Entity extends AggregateRoot<I$pascalCase$BuildProps> implements I$pascalCase$Entity {


  constructor(props : I$pascalCase$BuildProps){
    super(props);
  }

  public lean() : Required<I$pascalCase$EntityDto> {
    return JSON.parse(JSON.stringify({...this, _id:this._id }))
  }


}

export {$pascalCase$Builder}